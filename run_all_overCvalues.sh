#!/bin/bash

declare -a CONF_ARRAY
CONF_ARRAY=("10" "11")

declare -a EXPT_ARRAY
EXPT_ARRAY=("101" "102")

declare -a BANDIT_ARRAY
BANDIT_ARRAY=("50" "100" "150" "200" "250" "300")

NUM_BANDITS=${#BANDIT_ARRAY[@]}
NUMEXPTS=${#CONF_ARRAY[@]}


for ((i=0;i<$NUMEXPTS;i++)); do

CONF=${CONF_ARRAY[i]}
EXPT=${EXPT_ARRAY[i]}

GAMMA="0.9"
MAXLEVEL="0"
MAXDELTA="0.1"
MAXHORIZONS="10"
EPSILON="0.001"
MAXSTAGES="10"
MAXTRIALS="5"
SAMPLING_ERROR_BOUND="0.2"
ALPHA="0.05"
PARTICLE_COUNT="500"
TIME_BOUND="10000"
IS_RANDOM="false"

clear
echo "Running Experiment $EXPT, Config $CONF"

echo "Running NestedVI to build policy." 
echo "With params: Config - $CONF Gamma - $GAMMA Max Level - $MAXLEVEL Max Delta - $MAXDELTA Max Horizon - $MAXHORIZONS Epsione- $EPSILON Experiment - $EXPT"
mvn exec:java -Dexec.mainClass="nestedMDPSolver.NestedVI" -Dexec.args="$CONF $GAMMA $MAXLEVEL $MAXDELTA $MAXHORIZONS $EPSILON $EXPT" | tee -a NMDP_EXP"$EXPT"_CONFIG"$CONF"_MAXHORIZON"$MAXHORIZONS".log

echo "Running NestedVI Simulator" 
echo "With params: Config- $CONF Stages - $MAXSTAGES Trials - $MAXTRIALS Max Horizons - $MAXHORIZONS Experiment - $EXPT"
mvn exec:java -Dexec.mainClass="simulators.NestedVIBaselineSimulator" -Dexec.args="$CONF $MAXSTAGES $MAXTRIALS $MAXHORIZONS $EXPT $IS_RANDOM" | tee -a NESTEDVISIM_EXP"$EXPT"_CONFIG"$CONF"_MAXHORIZON"$MAXHORIZONS".log

echo "Running NOOP Simulator" 
echo "With params: Config- $CONF Max Stages- $MAXSTAGES Max Trials - $MAXTRIALS Experiment - $EXPT"
mvn exec:java -Dexec.mainClass="simulators.NOOPBaselineSimulator" -Dexec.args="$CONF $MAXSTAGES $MAXTRIALS $EXPT $IS_RANDOM" | tee -a NOOP_EXP"$EXPT"_CONFIG"$CONF".log

echo "Running Heuristic Simulator" 
echo "With params: Config- $CONF Max Stages- $MAXSTAGES Max Trials - $MAXTRIALS Experiment - $EXPT"
mvn exec:java -Dexec.mainClass="simulators.HeuristicBaselineSimulator" -Dexec.args="$CONF $MAXSTAGES $MAXTRIALS $EXPT $IS_RANDOM" | tee -a HEURISTIC_EXP"$EXPT"_CONFIG"$CONF".log

for((bandit=0;bandit<$NUM_BANDITS;bandit++));do
BANDIT_CONSTANT=${BANDIT_ARRAY[bandit]}

echo "Running IPOMCP Simulator" 
echo "With params: Config - $CONF Gamma - $GAMMA Epsilone - $EPSILON Max Horizon - $MAXHORIZONS Stages - $MAXSTAGES Trials - $MAXTRIALS Time Bound in Mili Second - $TIME_BOUND Experiment - $EXPT Sampling Error - $SAMPLING_ERROR_BOUND Alpha - $ALPHA Particle Count - $PARTICLE_COUNT Bandit Constant - $BANDIT_CONSTANT"
mvn exec:java -Dexec.mainClass="simulators.IPOMCPSimulator" -Dexec.args="$CONF $GAMMA $EPSILON $MAXHORIZONS $MAXSTAGES $MAXTRIALS $TIME_BOUND $EXPT $SAMPLING_ERROR_BOUND $ALPHA $PARTICLE_COUNT $BANDIT_CONSTANT  $IS_RANDOM" | tee -a IPOMCP_EXP"$EXPT"_CONFIG"$CONF"_MAXHORIZONS"$MAXHORIZONS".log


done


done
