#/bin/bash

declare -a CONF_ARRAY
CONF_ARRAY=("13")

declare -a EXPT_ARRAY
EXPT_ARRAY=("134")
NUMEXPTS=${#CONF_ARRAY[@]}


declare -a CUR_GROUP_ARRAY
CUR_GROUP_ARRAY=("0-4" "5-14" "15-19" "20-29")
NUMCUR=${#CUR_GROUP_ARRAY[@]}

declare -a NEIGHBOUR_GROUP_ARRAY
NEIGHBOUR_GROUP_ARRAY=("0-4" "5-14" "15-19" "20-29")
NUMNEIGHBOUR=${#CUR_GROUP_ARRAY[@]}



for ((i=0;i<$NUMEXPTS;i++)); do

CONF=${CONF_ARRAY[i]}
EXPT=${EXPT_ARRAY[i]}

GAMMA="0.9"
MAXLEVEL="0"
MAXDELTA="0.1"
MAXHORIZONS="10"
EPSILON="0.001"
MAXSTAGES="15"
MAXTRIALS="30"
ITERATIVE_TRIALS="10"
SETSOFTRIAL="3"
SAMPLING_ERROR_BOUND="0"
ALPHA="0"
PARTICLE_COUNT="1000"
BANDIT_CONSTANT="250"
TIME_BOUND="10000"
IS_FIRE_RANDOM="false"
IS_SUPP_RANDOM="true"
IS_AVGQ_ANODE="true"
QVALUE_ERROR_MARGIN="0.01"

clear
echo "Running Experiment $EXPT, Config $CONF"

echo "Running NestedVI to build policy." 
echo "With params: Config - $CONF Gamma - $GAMMA Max Level - $MAXLEVEL Max Delta - $MAXDELTA Max Horizon - $MAXHORIZONS Epsione- $EPSILON Experiment - $EXPT Q-Value Error $QVALUE_ERROR_MARGIN"
NESTEDVI_SCRIPT="echo \"RUN:\""

  for ((j=0;j<$NUMCUR;j++)); do
  CUR_GROUP=${CUR_GROUP_ARRAY[j]}

    for ((k=0;k<$NUMNEIGHBOUR;k++)); do
    NEIGHBOUR_GROUP=${NEIGHBOUR_GROUP_ARRAY[k]}

     NESTEDVI_SCRIPT="$NESTEDVI_SCRIPT & mvn exec:java -Dexec.mainClass=\"nestedMDPSolver.NestedVI\" -Dexec.args=\"$CONF $GAMMA $MAXLEVEL $MAXDELTA $MAXHORIZONS $EPSILON $EXPT $CUR_GROUP $NEIGHBOUR_GROUP $QVALUE_ERROR_MARGIN\""

    done
  done
echo $NESTEDVI_SCRIPT
eval $NESTEDVI_SCRIPT



echo "Running NestedVI Simulator" 
echo "With params: Config- $CONF Stages - $MAXSTAGES Trials - $MAXTRIALS Max Horizons - $MAXHORIZONS Experiment - $EXPT"
mvn exec:java -Dexec.mainClass="simulators.NestedVIBaselineSimulator" -Dexec.args="$CONF $MAXSTAGES $MAXTRIALS $MAXHORIZONS $EXPT $IS_FIRE_RANDOM $IS_SUPP_RANDOM" | tee -a NESTEDVISIM_EXP"$EXPT"_CONFIG"$CONF"_MAXHORIZON"$MAXHORIZONS".log

echo "Running NOOP Simulator" 
echo "With params: Config- $CONF Max Stages- $MAXSTAGES Max Trials - $MAXTRIALS Experiment - $EXPT"
mvn exec:java -Dexec.mainClass="simulators.NOOPBaselineSimulator" -Dexec.args="$CONF $MAXSTAGES $MAXTRIALS $EXPT $IS_FIRE_RANDOM $IS_SUPP_RANDOM" | tee -a NOOP_EXP"$EXPT"_CONFIG"$CONF".log

echo "Running Heuristic Simulator" 
echo "With params: Config- $CONF Max Stages- $MAXSTAGES Max Trials - $MAXTRIALS Experiment - $EXPT"
mvn exec:java -Dexec.mainClass="simulators.HeuristicBaselineSimulator" -Dexec.args="$CONF $MAXSTAGES $MAXTRIALS $EXPT $IS_FIRE_RANDOM $IS_SUPP_RANDOM" | tee -a HEURISTIC_EXP"$EXPT"_CONFIG"$CONF".log

#echo "Running IPOMCP Simulator"
#echo "With params: Config - $CONF Gamma - $GAMMA Epsilone - $EPSILON Max Horizon - $MAXHORIZONS Stages - $MAXSTAGES Trials - $MAXTRIALS Time Bound in Mili Second - $TIME_BOUND Experiment - $EXPT Sampling Error - $SAMPLING_ERROR_BOUND Alpha - $ALPHA Particle Count - $PARTICLE_COUNT Bandit Constant - $BANDIT_CONSTANT"


#IPOMCP_SCRIPT="echo \"RUN IPOMCP:\""
#for ((t=1;t<=$SETSOFTRIAL;t++)); do
#     IPOMCP_SCRIPT="$IPOMCP_SCRIPT & mvn exec:java -Dexec.mainClass=\"simulators.IPOMCPSimulator\" -Dexec.args=\"$CONF $GAMMA $EPSILON $MAXHORIZONS $MAXSTAGES $ITERATIVE_TRIALS $TIME_BOUND $EXPT $SAMPLING_ERROR_BOUND $ALPHA $PARTICLE_COUNT $BANDIT_CONSTANT $IS_FIRE_RANDOM $IS_SUPP_RANDOM $t\""
#done
#echo $IPOMCP_SCRIPT
#eval $IPOMCP_SCRIPT


done

